import { auth , database} from './firebase';
var hashHistory = require('react-router').hashHistory;
function requireAuth(nextState) {
	var directingToPath = nextState.routes[0].path; //gets the path the user wants to go to

	auth.onAuthStateChanged(function(user) { //check if user is logged in
		console.log('usr is ' + JSON.stringify(user));
		
	  	if (user == null) {
			  alert("not connected");
			  //hashHistory.push('/signin');
			  hashHistory.push('/login');
	  	}
	});
};



export function isLoggedIn()
{
	auth.onAuthStateChanged(function(user) { //check if user is logged in
		console.log('usr is ' + JSON.stringify(user));
		
	  	if (user == null) {
			  return false;
	  	}else{
			  return true;
		  }
	});

}

export default requireAuth;