import React from 'react'
import  {NotificationComponent} from '../../components/NotificationComponent';
import  {Experience}  from '../../components/ExperienceComponent';
import {Education} from '../../components/EducationComponent';
import axios from 'axios';
import { API_URL} from '../../APU_URL';
import{userService} from '../../services/user.service'; 
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Card,CardBody,CardText,CardSubtitle,CardTitle } from 'reactstrap';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import { ToastContainer, toast } from 'react-toastify';
import { auth , database} from '../../firebase';
export default class ModifyProfile extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      skills: [],
      modal: false,
      modal2:false,
      educations:[],
      experiences:[],
      headline :'',
      userProfile :{},
    //______________________________//
      newExperienceTitle:'',
      title :'',
      newExperienceCompany : '',
      newExperienceLocation : '',
      newExperienceStartDate : new Date(),
      newExperienceEndDate : new Date(),
      newExperienceDescription : '',

      //_____EDUCATION___________________//
     newEducationSchool :'',
     newEducationBac :'',
     newEducationFiled :'',
     newEducationStartYear :'',
     newEducationEndYear :'',



    };
   // this.handleChange =this.handleChange.bind(this);
    this.handleChange =this.handleChange.bind(this);
    this.handleSkills =this.handleSkills.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggle2 = this.toggle2.bind(this);
  }

 //________________________//
 componentWillMount(){

  var that = this;

  //______________GET THE CURRENTUSER INFORMATION____________________________//	
    auth.onAuthStateChanged(user =>{
        if(user){
            this.userRef = database.ref('users').child(auth.currentUser.uid);
            this.userRef.on("value", snap=>{
    
           
          
                console.log('skills' + JSON.stringify(snap.val().educations))
                //_____GETE THE DATA FROM USER PROFILE_____________________//
                let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
              'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid ,
            skills :snap.val().skills ,'headline' :snap.val().headline ,
          'experiences': snap.val().experiences ,'educations': snap.val().educations }
          
              //____________SET STATE________________________________//
              this.setState({userProfile:userProfile})
              this.setState({isLoggedIn:true});
              this.setState({skills:snap.val().skills});
              this.setState({experiences:snap.val().experiences});
              this.setState({educations:snap.val().educations});
            });
        }
    });//_____end auth StateChanged_________________//


  //__________________________________________________________________//




    
  }


//_____________________________________//

handleChange (evt) {
  // check it out: we get the evt.target.name (which will be either "email" or "password")
  // and use it to target the key on our `state` object with the same name, using bracket syntax
  this.setState({ [evt.target.name]: evt.target.value });
}  



  //_____________________________________//
  handleUserInput =(e) => {
		const name = e.target.name;
    const value = e.target.value;
    console.log('name' + name + 'value' + value);
		this.setState({[name]: value});
	  }	  

  validateField(fieldName, value) {
  let fieldValidationErrors = this.state.formErrors;
  let emailValid = this.state.emailValid;
  let passwordValid = this.state.passwordValid;
  
  switch(fieldName) {
    case 'email':
    emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    fieldValidationErrors.email = emailValid ? '' : ' is invalid';
    break;
    case 'password':
    passwordValid = value.length >= 6;
    fieldValidationErrors.password = passwordValid ? '': ' is too short';
    break;
    default:
    break;
  }
  this.setState({formErrors: fieldValidationErrors,
          emailValid: emailValid,
          passwordValid: passwordValid
          }, this.validateForm);
  }
  
  validateForm() {
  this.setState({formValid: this.state.emailValid && this.state.passwordValid});
  }

  errorClass(error) {
  return(error.length === 0 ? '' : 'has-error');
}



//___________________//
handleSkills(skills) {
  this.setState({skills})
}  

//_____________________________//
toggle() {
  this.setState({
    modal: !this.state.modal
  });
}

//_____________________________//
toggle2() {
  this.setState({
    modal2: !this.state.modal2
  });
}

//________________________//
AddSkills= ()=>
{
  //here send servcie to server skills____//

let{userId , skills} =this.state ;
console.log('skills' + skills);
let data ={ "skills" : skills}
let userRef = database.ref('users');
userRef
    .child(auth.currentUser.uid)
    .update(data)
    .then(() => userRef.once('value'))
    .then(snapshot => {
      snapshot.val() ;
      toast.success("You Have succesfuly Added the skills to your profile!", {
        position: toast.POSITION.TOP_CENTER
      });
    
    
    
    })
    .catch(error => ({
      errorCode: error.code,
      errorMessage: error.message
    } , 
    alert("error in updating your profile !")));
//this.setState({skills:[]})
}

//___________________________________//
//___________________________________//





AddExperience =()=>
{
  console.log('add experience!!')
  let{ newExperienceTitle ,newExperienceCompany, newExperienceDescription,
  newExperienceLocation, newExperienceStartDate , newExperienceEndDate}= this.state ;
  console.log( newExperienceTitle ,newExperienceCompany, newExperienceDescription,
    newExperienceLocation, newExperienceStartDate , newExperienceEndDate)
 


//_____________________________________________//

var NewExperienceData = {
  user_id: auth.currentUser.uid,
  user_name: auth.currentUser.first + ' ' +  auth.currentUser.last ,
  newExperienceTitle :newExperienceTitle,
  newExperienceCompany :newExperienceCompany,
  newExperienceDescription : newExperienceDescription,
  newExperienceLocation :newExperienceLocation,
  newExperienceStartDate: newExperienceStartDate ,
  newExperienceEndDate: newExperienceEndDate,
  created_at: new Date().getTime(),
 
};

var experiences =[];
experiences.push(NewExperienceData)
var data ={'experiences': experiences}

//generate new post reference key
/*var experienceRefKey = database.ref().child('experiences').push().key;
//sets the postData to the post child with the postRefKey
database.ref('experiences/' + experienceRefKey).set(NewExperienceData);
//sets the postData to the user-posts child with the currentUserId & the postRefKey
database.ref('/user-experiences/' + auth.currentUser.uid + '/' + experienceRefKey).set( NewExperienceData);*/

//____add the job offer ____//

//emptys the post text field
let userRef = database.ref('users');
  userRef
      .child(auth.currentUser.uid)
      .update(data)
      .then(() => userRef.once('value'))
      .then(snapshot => {
        snapshot.val() ;
        this.toggle();
        toast.success("You Have succesfuly Added the skills to your profile!", {
          position: toast.POSITION.TOP_CENTER
        });
      
      this.setState({newExperienceDescription:''})
      this.setState({newExperienceTitle:''})
      this.setState({newExperienceLocation:''})
      this.setState({newExperienceCompany:''})
      
      })
      .catch(error => ({
        errorCode: error.code,
        errorMessage: error.message
      } , 
      alert("error in updating your profile !")));




}




//____function to update personal info________________//

updatePersonalInfo =(e)=>{
  e.preventDefault();
 
  let { headline ,userProfile} = this.state ;

  let data ={
    'first:':userProfile.firstName,
    'last::':userProfile.lastName,
    'headline':headline
  }

let userRef = database.ref('users');
  userRef
      .child(auth.currentUser.uid)
      .update(data)
      .then(() => userRef.once('value'))
      .then(snapshot => {
        snapshot.val() ;
        toast.success("You Have succesfuly Added the skills to your profile!", {
          position: toast.POSITION.TOP_CENTER
        });
      
      
      
      })
      .catch(error => ({
        errorCode: error.code,
        errorMessage: error.message
      } , 
      alert("error in updating your profile !")));




}









//___function to add an education_________________//

AddEducation =(e)=>{
  e.preventDefault();

  console.log('add experience!!')
  let{ newExperienceTitle ,newExperienceCompany, newExperienceDescription,
  newExperienceLocation, newExperienceStartDate , newExperienceEndDate}= this.state ;
  console.log( newExperienceTitle ,newExperienceCompany, newExperienceDescription,
    newExperienceLocation, newExperienceStartDate , newExperienceEndDate)
 


//_____________________________________________//

var NewEducationData = {
  user_id: auth.currentUser.uid,
  user_name: auth.currentUser.first + ' ' +  auth.currentUser.last ,
  newExperienceTitle :newExperienceTitle,
  newExperienceCompany :newExperienceCompany,
  newExperienceDescription : newExperienceDescription,
  newExperienceLocation :newExperienceLocation,
  newExperienceStartDate: newExperienceStartDate ,
  newExperienceEndDate: newExperienceEndDate,
  created_at: new Date().getTime(),
 
};

var educations =[];
educations.push(NewEducationData)
var data ={'educations': educations}

//generate new post reference key
/*var experienceRefKey = database.ref().child('experiences').push().key;
//sets the postData to the post child with the postRefKey
database.ref('experiences/' + experienceRefKey).set(NewExperienceData);
//sets the postData to the user-posts child with the currentUserId & the postRefKey
database.ref('/user-experiences/' + auth.currentUser.uid + '/' + experienceRefKey).set( NewExperienceData);*/

//____add the job offer ____//

//emptys the post text field
let userRef = database.ref('users');
  userRef
      .child(auth.currentUser.uid)
      .update(data)
      .then(() => userRef.once('value'))
      .then(snapshot => {
        snapshot.val() ;
        this.toggle2();
        toast.success("You Have succesfuly Added Education to your profile", {
          position: toast.POSITION.TOP_CENTER
        });
      
      this.setState({newExperienceDescription:''})
      this.setState({newExperienceTitle:''})
      this.setState({newExperienceLocation:''})
      this.setState({newExperienceCompany:''})
      
      })
      .catch(error => ({
        errorCode: error.code,
        errorMessage: error.message
      } , 
      alert("error in updating your profile !")));

  
}




  render() {

  //____get the skills____//
  let {skills , userProfile ,experiences , educations}=this.state;  



let educ;
let exp ;
if(experiences.length>0)
{
  
  exp= experiences.map((experience) =>
  <Experience experience={experience} />
);


}else{
  exp=<h1>No Experience Yet </h1>
}

//____render the ecuations_____________//
/*if(educations && educations.length>0)
{
  
  educ= userProfile.experiencesnumbers.map((education) =>
  <Education education={education}  />
);


}else{
  educ=<h1>No Educations Yet </h1>
}*/


  //____the experiences_____________//

    return (
      <div className="container">

<div class="row">
  <div class="col-3">
  <div>
  <Card body className="text-center">
  <form onSubmit={this.updatePersonalInfo}>


  <div class="form-group">
    <label for="exampleInputEmail1">Headline</label>
    <input type="text" 
    name="headline"
    value={this.state.userProfile.headline}  onChange={(event) => this.handleUserInput(event)}  class="form-control" placeholder="Enter Your Headline" />
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">First Name</label>
    <input onChange={(event) => this.handleUserInput(event)} value={this.state.userProfile.firstName} type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Name" />
  </div>


  <div class="form-group">
    <label for="exampleInputEmail1">Last Name</label>
    <input onChange={(event) => this.handleUserInput(event)} value={this.state.userProfile.lastName} type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Last Name" />
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input onChange={(event) => this.handleUserInput(event)} value={this.state.userProfile.email} type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
  </div>
 
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
  </Card>
    </div>
  </div>
  <div class="col-6">

  <div className="row">
  <button onClick={this.toggle}  class="btn btn-primary">Add Experience</button>
  </div>
  <br/>
  

  <div className="row">

  
{  exp }

 <hr/>
 <hr/>
<br/>

  

  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Experience </ModalHeader>
          <ModalBody>
          
          <div class="post-project-fields">
              <form>
                 <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" placeholder="ex Fullstack Developer" 
                   value={this.state.newExperienceTitle} name="newExperienceTitle" onChange={event => this.handleChange(event)} />
                </div>

              

                <div class="form-group">
                    <label for="company">Company</label>
                    <input type="text" class="form-control" id="company" placeholder="Company"
                    required value={this.state.newExperienceCompany}  name="newExperienceCompany" onChange={(event) => this.handleUserInput(event)}/>
                  </div>



                  <div class="form-group">
                      <label for="Location">Location</label>
                      <input type="text" value={this.state.newExperienceLocation} name="newExperienceLocation"  onChange={(event) => this.handleUserInput(event)} required  class="form-control" id="Location" placeholder="Location" />
                    </div>


                    <div class="form-group">
                        <label for="startdate" >Start Month</label>
                        <select value={this.state.newExperienceStartDate}  class="form-control" onChange={(event) => this.handleUserInput(event)} name="date_begin"
                         name="newExperienceStartDate">
                          <option value="January">January</option>
                          <option value="June">June</option>
                        </select>
                      </div>

                     

                      <div class="form-check">
                          <input onChange={(event) => this.handleUserInput(event)} name ="currenltyWorking" class="form-check-input" 
                          type="checkbox" value="" id="defaultCheck1" />
                          <label class="form-check-label" for="defaultCheck1">
                            I currently work here 
                          </label>
                        </div>
                        <br />
                        <br />
                        <br />


                        <div class="form-group">
                            <label for="startdate">End Month</label>
                            <select  value={this.state.newExperienceEndDate} name="newExperienceEndDate" onChange={(event) => this.handleUserInput(event)} class="form-control" name="date_end"
                             id="endmonth">
                              <option value="January">January</option>
                              <option value="June">June</option>
                            </select>
                          </div>
    
    
    
                        
                      <div class="form-group">
                          <label for="description">Description</label>
                          <textarea value={this.state.newExperienceDescription}  onChange={(event) => this.handleUserInput(event)}  
                           class="form-control" name="newExperienceDescription" rows="3"></textarea>
                        </div>

                
                
              </form>
            </div>{/*<!--post-project-fields end-->*/}

             
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.AddExperience}>Post</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
</Modal>
  </div>

<br/>
<br/>

  <div className="row">
  <button onClick={this.toggle2}  class="btn btn-primary">Add Education</button>
  </div>
  <br/>
{/*educations*/}

<Modal isOpen={this.state.modal2} toggle={this.toggle2} className={this.props.className}>
        <ModalHeader toggle={this.toggle2}>Education </ModalHeader>
        <ModalBody>
        
        <div class="post-project-fields">
            <form>
            <div class="form-group">
                    <label for="title">School</label>
                    <input onChange={(event) => this.handleUserInput(event)} type="text" class="form-control" placeholder="School Name" 
                    id="shcool" name="school" 
                    required
                    
                     />
                  </div>
  
                  <div class="form-group">
                      <label for="company">Degree</label>
                      <input onChange={(event) => this.handleUserInput(event)}  type="text" class="form-control" id="degree" placeholder="Bachelor" />
                    </div>
  
  
  
                    <div class="form-group">
                        <label for="Location">Filed of study</label>
                        <input 
                        name="field_study" type="text" class="form-control" id="field" 
                        placeholder="Computer science"  onChange={(event) => this.handleUserInput(event)}/>
                      </div>
  
  
                      <div class="form-group">
                          <label for="startdate">From Year </label>
                          <select
                          name="field_study" onChange={(event) => this.handleUserInput(event)}  class="form-control" id="startdate">
                            <option value="2012">2012</option>
                            <option value="2013">2013</option>
                          </select>
                        </div>


                        <div class="form-group">
                            <label for="startdate">To Year </label>
                            <select
                            name="end_year" onChange={(event) => this.handleUserInput(event)} class="form-control" id="enddate">
                              <option value="2012">2012</option>
                              <option value="2013">2013</option>
                            </select>
                          </div>
              
              
            </form>
          </div>{/*<!--post-project-fields end-->*/}

           
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.AddEducation}>Post</Button>{' '}
          <Button color="secondary" onClick={this.toggle2}>Cancel</Button>
        </ModalFooter>
</Modal>
  </div>
 

  <div class="col-3">
  <Card body className="text-center">
  <CardTitle>Skills</CardTitle>
  <ul className="skill-tags">
	
    {skills.map((skill, i) => {     
                            
           // Return the element. Also pass key     
           return (	<li><a href="#" title="">{skill}</a></li>) 
        })}	
	</ul>
  <TagsInput value={this.state.skills} onChange={this.handleSkills} />
  <Button color="primary" onClick={this.AddSkills}>Add</Button>{' '}
  </Card>
  </div>
</div>
   </div>
  

    );
    
  }
}


