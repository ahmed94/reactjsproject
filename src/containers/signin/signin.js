import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {FormErrors} from './FormErrors';
import { history} from '../../_helpers/history';
import{userService} from '../../services/user.service'; 
import axios from 'axios' ;
import { auth , database} from '../../firebase';

export default class SignInPage extends React.Component {



	constructor (props) {
		super(props);
		this.state = {
		  email: '',
		  password: '',
		  formErrors: {email: '', password: ''},
		  emailValid: false,
		  passwordValid: false,
		  formValid: false
		}
	  }





	  handleUserInput (e) {
		const name = e.target.name;
		const value = e.target.value;
		this.setState({[name]: value}, 
					  () => { this.validateField(name, value) });
	  }	  

	  validateField(fieldName, value) {
		let fieldValidationErrors = this.state.formErrors;
		let emailValid = this.state.emailValid;
		let passwordValid = this.state.passwordValid;
	  
		switch(fieldName) {
		  case 'email':
			emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
			fieldValidationErrors.email = emailValid ? '' : ' is invalid';
			break;
		  case 'password':
			passwordValid = value.length >= 6;
			fieldValidationErrors.password = passwordValid ? '': ' is too short';
			break;
		  default:
			break;
		}
		this.setState({formErrors: fieldValidationErrors,
						emailValid: emailValid,
						passwordValid: passwordValid
					  }, this.validateForm);
	  }
	  
	  validateForm() {
		this.setState({formValid: this.state.emailValid && this.state.passwordValid});
	  }

	  errorClass(error) {
		return(error.length === 0 ? '' : 'has-error');
	}

	handleSubmit=(event) => {
		//alert('A name was submitted: ');
		//____here send to server ____//
		event.preventDefault();
		let {email, password} = this.state ;
		var that = this ;
	


	auth.signInWithEmailAndPassword(email, password)
	.then(data=>{
		alert("use sign in with success !!" + JSON.stringify(data));
		this.props.history.push('/');
	})
	.catch(function(error) {
		var errorCode = error.code;
		var errorMessage = error.message;

		alert(error.message);

		//sets hasError and the errorMsg if an error occured to show in the alerts
		if(error){
			that.setState({hasError: true});
			that.setState({errorMsg: "Invalid email or password combination."});
		}
})
		
	





		
	  }


  render() {


    return (
      <div className="sign-in-page">
			<div className="signin-popup">
				<div className="signin-pop">
					<div className="row">
						<div className="col-lg-6">
							<div className="cmp-info">
								<div className="cm-logo">
									<img src="images/cm-logo.png" alt="" />
									<p>meetPro is platform bla bla bla bla bla ahahhahahaha
										sksksksksks kakakakak
									</p>
								</div>	
								<img src="images/cm-main-img.png" alt="" />			
							</div>
						</div>
						<div className="col-lg-6">
							<div className="login-sec">
								<ul className="sign-control">
									<li data-tab="tab-1" className="current"><Link to="/">Sign in</Link></li>				
									<li data-tab="tab-2"><Link to="/signup">Sign up</Link></li>				
								</ul>			
								<div className="sign_in_sec current" id="tab-1">
									
									<h3>Sign in</h3>
									<form ref="form" onSubmit={this.handleSubmit}>

										<div className="panel panel-default">
                                          <FormErrors formErrors={this.state.formErrors} />
                                           </div>
										<div className="row">
											<div className="col-lg-12 no-pdd" className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
												<div className="sn-field" >
													<input type="email"
													onChange={(event) => this.handleUserInput(event)}
													value={this.state.email} name="email" placeholder="Email" />
													<i className="la la-user"></i>
												</div>
											</div>
											<div className="col-lg-12 no-pdd" className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
												<div className="sn-field">
													<input onChange={(event) => this.handleUserInput(event)} value={this.state.password} type="password" name="password" placeholder="Password" />
													<i className="la la-lock"></i>
												</div>
											</div>
											<div className="col-lg-12 no-pdd">
												<div className="checky-sec">
													<div className="fgt-sec">
														<input type="checkbox" name="cc" id="c1" />
														<label htmlFor="c1">
															<span></span>
														</label>
														<small>Remember me</small>
													</div>
												
												</div>
											</div>
											<div className="col-lg-12 no-pdd">
												<button 
												  disabled={!this.state.formValid} type="button"
												  onClick={this.handleSubmit} value="submit">Sign in</button>
											</div>
										</div>
									</form>

									
								
								</div>
								<div className="sign_in_sec" id="tab-2">
									<div className="signup-tab">
										<i className="fa fa-long-arrow-left"></i>
										<h2>johndoe@example.com</h2>
										<ul>
											<li data-tab="tab-3" className="current"><a href="#" title="">User</a></li>
											<li data-tab="tab-4"><a href="#" title="">Company</a></li>
										</ul>
									</div>
								
								
								</div>		
							</div>{/* A JSX comment */}
						</div>
					</div>		
				</div>{/* A JSX comment */}
      </div>{/* A JSX comment */}
      </div>

    );
    ;
  }
}

/*const mapStateToProps = ({ counter }) => ({
  count: counter.count,
  isIncrementing: counter.isIncrementing,
  isDecrementing: counter.isDecrementing
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      increment,
      incrementAsync,
      decrement,
      decrementAsync,
      changePage: () => push('/about-us')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)*/
