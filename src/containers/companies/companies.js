import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import  CreateCompanyPage   from '../../components/createCompanyPage';
import {Company } from '../../components/CompanyComponent';
import{userService} from '../../services/user.service'; 
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { auth , database} from '../../firebase';
import _ from 'lodash';


export default class Companies extends React.Component {


	constructor(props) {
		super(props);
		this.state = {
		  companies:[],
			profile :{},
			currentUserID: "",

		  
		};

	   
	  }
		componentWillMount(){



	//______________GET THE CURRENTUSER INFORMATION____________________________//	
		auth.onAuthStateChanged(user =>{
				if(user){
						this.userRef = database.ref('users').child(auth.currentUser.uid);
						this.userRef.on("value", snap=>{
		
					 
					
								this.setState({username: snap.val().first + " " + snap.val().last});
								//_____GETE THE DATA FROM USER PROFILE_____________________//
								let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
							'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid }
							//____________SET STATE________________________________//
							this.setState({public_profile:userProfile})
						});
				}
		});
		
		
		
		//____reference to this____________//
		var that = this;
						//gets the company reference
		this.companiesRef = database.ref().child('companies').orderByChild("created_at");
		
		//___________________load the companies__________________//
		this.companiesRef.on("child_added", snap => {
			var company = snap.val();
			company.company_id = snap.ref.key;
			company.user_imgurl = "https://firebasestorage.googleapis.com/v0/b/testingproject-cd660.appspot.com/o/images%2Fdefault.jpg?alt=media&token=23d9c5ea-1380-4bd2-94bc-1166a83953b7";
		
		 //_____set the posts from database________________// 
			var updatedPCompaniesArray = this.state.companies;
			updatedPCompaniesArray.push(company);
			this.setState({companies :  updatedPCompaniesArray});
		
			//____get the currentUser companies!!________________//
		
			var userRef = database.ref('users/'+ company.user_id);
			userRef.once('value', snap=>{
		
					company.user_imgurl = snap.val().imageURL;
		
					var index = -1;
					for(var i = 0; i < this.state.companies.length; i++){
							if(this.state.companies[i].post_id == company.company_id){
									index = i;
							}
					}
		
					var updatedPCompaniesArray = this.state.companies;
					updatedPCompaniesArray.splice(index, 1, company);
					this.setState({companies: updatedPCompaniesArray});
			});
		});


		}
	
		componentWillUnmount(){
			//this.companyRef.off();
	}

	
	  CreateCompanyPage =(data)=>
	  {
		  //____send to the API__________________//
  /*userService.createCompanyPage(data)
  .then(res=>{
    console.log('post create duscesfuly');
    this.toggle();
    toast.success("Your Post published Succesfuly", {
      position: toast.POSITION.TOP_CENTER
    });

  }).catch(err=>{
    console.log('err creating post' + err);
    toast.error("Error creating Company Page !", {
      position: toast.POSITION.TOP_CENTER
    });
  })*/

		}
		


	 //___________Render three companies pages  in one Row_________________//
	 
	 renderCompanies() { 
		// two albums at a time - the current and previous item
		/*console.log('index is ' + index);
    let companies = [this.state.companies[index - 1], this.state.companies[index]]; */
    let {companies} =  this.state;
   /* return (
      <div className="columns" key={index}>
        {companies.map( (company,index) => {
          return (
					 <Company company={company} key={index} />
          );
        })}
      </div>
		);*/
		const rows =[];
		companies.map((company,i)=>{
			
     rows.push(<Company company={company} key={Math.random()} />)
		})

	
		
		//var rows = [<div className="col-md-4">content</div>, <div className="col-md-4">content</div>, <div className="col-md-4">content</div>]

  return _.chunk(rows, 3).map(function(group) { 


      return <div className="row">{group}</div>
		});
		
		
  }



	handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit = (event) => {
    //alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
    
  
   let {jobs,value} = this.state ;
   const results =[];
   //console.log('jobs are' + JSON.stringify(jobs));
   jobs.map((data,index)=>{
     console.log('hjob offer' + data + 'index' + index);

     if(data.companyName.toLowerCase().indexOf(value.toLowerCase())>=0 || data.description.toLowerCase().indexOf(value.toLowerCase())>=0
      )

     {
       
       alert("ther eis data" + JSON.stringify(results));       //add to result 
       results.push(data);
     }
   })

   this.setState({jobs:results});

  }


	followCompany =()=>
	{
		toast.success("You Have succesfuly Followed this company !", {
			position: toast.POSITION.TOP_CENTER
		});
	}


	componentWillUnmount() {
    //this.socket.close()
    this.companiesRef.off();
 }







  render() {

		let { companies} = this.state ;
		let companiesDisplay ;

		if(companies.length >0)
		{
			/*{companies.map((album, index) => {
				// use the modulus operator to determine even items return index % 2 ?
				this.renderAlbums(index) : '';
			})}*/
		}
    return (
      <section className="companies-info">
			<div className="container">


      <CreateCompanyPage CreateCompanyPage={this.createCompanyPage} />

			<div className="search-sec">
			<div className="container">
				<div className="search-box">
					<form onSubmit={this.handleSubmit}>
						<input value={this.state.value} onChange={this.handleChange}type="text" name="search" placeholder="Search Companies" />
						<button type="submit">Search</button>
					</form>
				</div>{/*<!--search-box end-->*/}
			</div>
		</div>{/*<!--search-sec end-->*/} 


				<div className="company-title">
					<h3>All Companies</h3>
				</div>{/*<!--company-title end-->*/}


				<div className="companies-list">

				
					 {this.renderCompanies()}
				


				</div>{/*<!--companies-list end-->*/}



				<div className="process-comm">
					<div className="spinner">
						<div className="bounce1"></div>
						<div className="bounce2"></div>
						<div className="bounce3"></div>
					</div>
        </div>{/*<!--process-comm end-->*/}
        

			</div>
		</section>
    );
    ;
  }
}

