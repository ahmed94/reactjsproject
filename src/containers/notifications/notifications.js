import React from 'react'
import  {NotificationComponent} from '../../components/NotificationComponent';
import axios from 'axios';
import { API_URL} from '../../APU_URL';
import{userService} from '../../services/user.service'; 
import { auth , database} from '../../firebase';

export default class Notifications extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      notifications:[]
    };

   
  }

 


 //________________________//
  componentWillMount(){



    //______________GET THE CURRENTUSER INFORMATION____________________________//	
      auth.onAuthStateChanged(user =>{
          if(user){
              this.userRef = database.ref('users').child(auth.currentUser.uid);
              this.userRef.on("value", snap=>{
      
             
            
                  this.setState({username: snap.val().first + " " + snap.val().last});
                  //_____GETE THE DATA FROM USER PROFILE_____________________//
                  let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
                'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid }
                //____________SET STATE________________________________//
                this.setState({username:snap.val().first + ' ' + snap.val().last })
                this.setState({isLoggedIn:true});
              });


     //______________________________________________________________________//

     //____reference to this____________//
var that = this;
//gets the job reference
this.notifsRef = database.ref().child('user-notifications/' +auth.currentUser.uid).orderByChild("created_at");

//___________________load the jobs__________________//
this.notifsRef.on("child_added", snap => {

var notif = snap.val();
notif.notif_id = snap.ref.key;
//job.user_imgurl = "https://firebasestorage.googleapis.com/v0/b/testingproject-cd660.appspot.com/o/images%2Fdefault.jpg?alt=media&token=23d9c5ea-1380-4bd2-94bc-1166a83953b7";

//_____set the jobs from database________________// 
var updatedNotifArray = this.state.notifications;
updatedNotifArray.push(notif);
this.setState({notifications : updatedNotifArray});


});



          }
      });

   //_____LISTEN FOR THE NOTIFICATIONS_______________________________//

  



      
    }









  render() {

    let{ notifications} =this.state ;

   
    let notif ;

    if(notifications.length >0)
    {
      notif = notifications.map((not) =>
        <NotificationComponent notification={not} />
      )
    }else{
      notif =<h1>There is No notifications Now!</h1>
    }

    return (
      <div className="container">
    <div className="row">

        <div className="company-title">
            <h3>Notfications!</h3>
          </div>{/*<!--company-title end-->*/}
      <div className="col-lg-6">

        

<div className="tab-pane" id="nav-notification" role="tabpanel" aria-labelledby="nav-notification-tab">
  <div className="acc-setting">
    <h3>Notifications</h3>
    <div className="notifications-list">

   {notif}
   



      


    
    
    
   
    
     

    </div>{/*<!--notifications-list end-->*/}


    </div>
</div>


</div>
</div>
</div>

    );
    
  }
}


