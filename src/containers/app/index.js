import React from 'react'
import Header from '../../components/header';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { auth , database} from '../../firebase';
import {Redirect} from 'react-router-dom'




export default class App extends React.Component {


  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    
    this.state = {
      dropdownOpen: false,
      public_profile :{},
      isLoggedIn :false,
      username :''
    };
    //______________________________________//
     //this.socket = io(URL_SOCKET);
     //this.socket = io('http://localhost:3000/');
  }


  //________________________//
  componentWillMount(){



    //______________GET THE CURRENTUSER INFORMATION____________________________//	
      auth.onAuthStateChanged(user =>{
          if(user){
              this.userRef = database.ref('users').child(auth.currentUser.uid);
              this.userRef.on("value", snap=>{
      
             
            
                  this.setState({username: snap.val().first + " " + snap.val().last});
                  //_____GETE THE DATA FROM USER PROFILE_____________________//
                  let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
                'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid }
                //____________SET STATE________________________________//
                this.setState({username:snap.val().first + ' ' + snap.val().last })
                this.setState({isLoggedIn:true});
                this.setState({userProfile:userProfile});
              });

              //__________________________________________________________________//
              this.notificationsRef = database.ref('user-notifications/'+ auth.currentUser.uid).orderByChild("created_at");
              this.notificationsRef.on("child_added", snap=>{
                let notification = snap.val();
                let data =JSON.parse(notification.forUser);
                console.log(data.email ===this.state.userProfile.email)
                
                if( data.email ===this.state.userProfile.email)
                {
                  /*toast.success(data.content, {
                    position: toast.POSITION.TOP_CENTER
                  });*/
                 alert("new notification")
                }
                //company.user_id = snap.ref.key;
                //this.state.companies.push(company);
                //this.setState({companies: this.state.companies});
              });
          }
      });

   //_____LISTEN FOR THE NOTIFICATIONS_______________________________//


   //console.log('auth currntuser' + auth.currentUser.uid);*/
  




      
    }



  toggle() {
    console.log('changed')
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }


  logout =() =>
  {
    auth.signOut();
    //this.props.history.push('/')
    window.location='/signin'
    return <Redirect to="/signin" push={true} />
  }


  

  render() {
    const baseUrl = process.env.PUBLIC_URL;
    let{ username, isLoggedIn}=this.state ;

    return (
      <div>
    { isLoggedIn && <Header logout={this.logout}
    username={username} isOpen={this.state.dropdownOpen} toggle={this.toggle} /> }

    <main>
    <ToastContainer autoClose={8000} />
   
      {this.props.children}

    </main>
  </div>
    );
  }

}

