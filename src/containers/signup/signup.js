import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {FormErrors} from './FormErrors';

import{userService} from '../../services/user.service'; 
import { auth , database} from '../../firebase';
import {
  HashRouter
} from 'react-router-dom';
import history from '../../history'
import { withRouter } from 'react-router-dom'

 export default class SignUpPage extends React.Component {



	constructor (props) {
		super(props);
		this.state = {
		  email: '',
      password: '',
      firstName :'',
      lastName:'',
		  formErrors: {email: '', password: ''},
		  emailValid: false,
		  passwordValid: false,
			formValid: false,
			hasError: false
		}
	  }





	  handleUserInput (e) {
		const name = e.target.name;
		const value = e.target.value;
		this.setState({[name]: value}, 
					  () => { this.validateField(name, value) });
	  }	  

	  validateField(fieldName, value) {
		let fieldValidationErrors = this.state.formErrors;
		let emailValid = this.state.emailValid;
		let passwordValid = this.state.passwordValid;
	  
		switch(fieldName) {
		  case 'email':
			emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
			fieldValidationErrors.email = emailValid ? '' : ' is invalid';

			break;
		  case 'password':
			passwordValid = value.length >= 6;
			fieldValidationErrors.password = passwordValid ? '': ' is too short';
			break;
		  default:
			break;
		}
		this.setState({formErrors: fieldValidationErrors,
						emailValid: emailValid,
						passwordValid: passwordValid
					  }, this.validateForm);
	  }
	  
	  validateForm() {
		this.setState({formValid: this.state.emailValid && this.state.passwordValid});
	  }

	  errorClass(error) {
		return(error.length === 0 ? '' : 'has-error');
	}

	handleSubmit=(event) => {
		var that = this;
		//____here send to server ____//
		event.preventDefault();



		
	//_____NEW AUTH with firebase________________________//
	
	auth.createUserWithEmailAndPassword(this.state.email,this.state.password)
	
	.then(data=>{
		console.log('user created succesfuly ');
	
	
	//___________________________________________//
	auth.onAuthStateChanged(function(user) {
		if (user) {
			var userData = {
				email: that.state.email,
				first: that.state.firstName,
				last: that.state.lastName,
				imageUrl : "https://ui-avatars.com/api/?name="+that.state.firstName,
				//imageURL: "https://firebasestorage.googleapis.com/v0/b/testingproject-cd660.appspot.com/o/images%2Fdefault.jpg?alt=media&token=23d9c5ea-1380-4bd2-94bc-1166a83953b7",
				interests: "",
				skills: "",
				educations :[{"":""}],
				experiences :[{"":""}]
			};
			
	
		database.ref('users/' + auth.currentUser.uid).set(userData);
	
		
		console.log('update profile' + 	user.updateProfile({
			displayName: that.state.firstName + " " + that.state.lastName,
		}))
	
			//update display name for user
			user.updateProfile({
				displayName: this.state.firstName + " " + this.state.lastName,
			});
	
			console.log('the user will be' + JSON.stringify(user));
	
			that.props.history.push('/modifyProfile');
		}
	});

	})
	
	
	
	.catch(function(error) {
		if(error){
			that.setState({hasError: true});
			that.setState({errorMsg: "Please enter a valid email address with a password of at least 6 characters."});
		}
		alert(error);
})



	
		}
		

		componentWillUnmount(){
			if (typeof this.unsubscribe == 'function')
			{
				this.unsubscribe();
			}
	}


  render() {


    return (
      <div className="sign-in-page">
			<div className="signin-popup">
				<div className="signin-pop">
					<div className="row">
						<div className="col-lg-6">
							<div className="cmp-info">
								<div className="cm-logo">
									<img src="images/cm-logo.png" alt="" />
									<p>meetPro is platform 
									</p>
								</div>	
								<img src="images/cm-main-img.png" alt="" />			
							</div>
						</div>
						<div className="col-lg-6">
							<div className="login-sec">
								<ul className="sign-control">
									<li data-tab="tab-1" ><Link to="/">Sign in</Link></li>				
									<li data-tab="tab-2" className="current"><Link to="/signup">Sign up</Link></li>				
								</ul>			
								<div className="sign_in_sec current" id="tab-1">
									
									<h3>Sign Up</h3>
									<form ref="form" onSubmit={this.handleSubmit}>

										<div className="panel panel-default">
                                          <FormErrors formErrors={this.state.formErrors} />
                                           </div>
										<div className="row">

										<div className="col-lg-12 no-pdd" className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
												<div className="sn-field" >
													<input type="text"
													onChange={(event) => this.handleUserInput(event)}
													value={this.state.firstName} name="firstName" placeholder="First Name" />
													<i className="la la-user"></i>
												</div>
											</div>



                      <div className="col-lg-12 no-pdd" className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
												<div className="sn-field" >
													<input type="text"
													onChange={(event) => this.handleUserInput(event)}
													value={this.state.lastName} name="lastName" placeholder="Last Name" />
													<i className="la la-user"></i>
												</div>
											</div>




											<div className="col-lg-12 no-pdd" className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
												<div className="sn-field" >
													<input type="email"
													onChange={(event) => this.handleUserInput(event)}
													value={this.state.email} name="email" placeholder="Email" />
													<i className="la la-user"></i>
												</div>
											</div>
											<div className="col-lg-12 no-pdd" className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
												<div className="sn-field">
													<input onChange={(event) => this.handleUserInput(event)} value={this.state.password} type="password" name="password" placeholder="Password" />
													<i className="la la-lock"></i>
												</div>
											</div>
											<div className="col-lg-12 no-pdd">
												<div className="checky-sec">
													<div className="fgt-sec">
														<input type="checkbox" name="cc" id="c1" />
														<label for="c1">
															<span></span>
														</label>
														<small>Remember me</small>
													</div>
												
												</div>
											</div>
											<div className="col-lg-12 no-pdd">
												<button 
												  disabled={!this.state.formValid} type="button"
												  onClick={this.handleSubmit} value="submit">Sign Up</button>
											</div>
										</div>
									</form>

									
								
								</div>
									
							</div>{/* A JSX comment */}
						</div>
					</div>		
				</div>{/* A JSX comment */}
      </div>{/* A JSX comment */}
      </div>

    );
    ;
  }
}

/*const mapStateToProps = ({ counter }) => ({
  count: counter.count,
  isIncrementing: counter.isIncrementing,
  isDecrementing: counter.isDecrementing
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      increment,
      incrementAsync,
      decrement,
      decrementAsync,
      changePage: () => push('/about-us')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)*/
//export default withRouter(SignUpPage);