import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { JobOffer } from '../../components/jobOffer';
import  CreateOffer   from '../../components/createJobOffer';
import { PublicProfile } from '../../components/PublicProfileComponent';
import{userService} from '../../services/user.service'; 
import { auth , database} from '../../firebase';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import lunr from 'lunr';


export default class Jobs extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      jobs:[],
      value: '',
      public_profile :{}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

   

   /* this.idx = lunr(function () {
      this.ref('job_id')
      this.field('description')
      this.field('tags')
      this.field('title')
    
      /*documents.forEach(function (doc) {
        this.add(doc)
      }, this)
    })*/

  //__________________________________________//
  




    
  }



 

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit = (event) => {
    //alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
    
  
   let {jobs,value} = this.state ;
   const results =[];
   //console.log('jobs are' + JSON.stringify(jobs));
   jobs.map((data,index)=>{
     console.log('hjob offer' + data + 'index' + index);

     if(data.title.toLowerCase().indexOf(value.toLocaleLowerCase())>=0 || data.description.toLowerCase().indexOf(value.toLocaleLowerCase())>=0
       )

     {
       
       //add to result 
       results.push(data);
     }
   })
   if(results.length>0)
   {
     alert("there are results");
     
   }

   this.setState({jobs:results});

  }



//______________________________________________________//



componentWillMount() {

  




  //____GET THE currentUser username____________________//


auth.onAuthStateChanged(user =>{
    if(user){
        this.userRef = database.ref('users').child(auth.currentUser.uid);
        this.userRef.on("value", snap=>{

       
      
            this.setState({username: snap.val().first + " " + snap.val().last});
            //_____GETE THE DATA FROM USER PROFILE_____________________//
            let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
          'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid }
          //____________SET STATE________________________________//
          this.setState({public_profile:userProfile})
        });
    }
});



//____reference to this____________//
var that = this;
        //gets the job reference
this.jobsRef = database.ref().child('jobs').orderByChild("created_at");

//___________________load the jobs__________________//
this.jobsRef.on("child_added", snap => {

  var job = snap.val();
  job.job_id = snap.ref.key;
  job.user_imgurl = "https://firebasestorage.googleapis.com/v0/b/testingproject-cd660.appspot.com/o/images%2Fdefault.jpg?alt=media&token=23d9c5ea-1380-4bd2-94bc-1166a83953b7";

 //_____set the jobs from database________________// 
  var updatedjobArray = this.state.jobs;
  updatedjobArray.push(job);
  this.setState({jobs : updatedjobArray});

  //____get the currentUser jobs________________//

  var userRef = database.ref('users/'+ job.user_id);
  userRef.once('value', snap=>{

      job.user_imgurl = snap.val().imageURL;

      var index = -1;
      for(var i = 0; i < this.state.jobs.length; i++){
          if(this.state.jobs[i].job_id == job.job_id){
              index = i;
          }
      }

      var updatedjobArray = this.state.jobs;
      updatedjobArray.splice(index, 1, job);
      this.setState({jobs: updatedjobArray});
  });
});




 //__________for each child changed to job, replace that job with the job already in jobArray
 this.jobsRef.on("child_changed", snap => {
  var job = snap.val();
  job.job_id = snap.ref.key;

  var userRef = database.ref('users/'+ job.user_id);
  userRef.once('value', snap=>{

      job.user_imgurl = snap.val().imageURL;

      var index;
      for(var i = 0; i < this.state.jobArray.length; i++){
          if(this.state.jobArray[i].job_id == job.job_id){
              index = i;
          }
      }

      var updatedjobArray = this.state.jobArray;
      updatedjobArray.splice(index, 1, job);
      this.setState({jobArray: updatedjobArray});
  });
});



  }



  componentWillUnmount() {
    //this.socket.close()
    this.jobsRef.off();
 }











//_____ApplyJOb____________________//

ApplyJob =(idOffer)=>{
  toast.success("You Have succesfuly Applied to the Job!", {
    position: toast.POSITION.TOP_CENTER
  });
}




  render() {
    let {jobs} =this.state ;
   let listJobs ;
    if(jobs.length>0)
    {
      listJobs = jobs.map((job,index) =>
      <JobOffer key={index} job={job} ApplyJob={this.ApplyJob} />
     
    );

    }else{
      listJobs= <h1>There is nos jobs to be displayed </h1>
    }
    return (
      <div className="container">

      <div className="row">
  
    <div className="col-8">

     <CreateOffer imageUrl={this.state.public_profile.imageUrl} />
    <div className="search-sec">
			<div className="container">
				<div className="search-box">
					<form onSubmit={this.handleSubmit}>
						<input value={this.state.value} onChange={this.handleChange}type="text" name="search" placeholder="Search keywords" />
						<button type="submit">Search</button>
					</form>
				</div>{/*<!--search-box end-->*/}
			</div>
		</div>{/*<!--search-sec end-->*/} 




    <div className="jobs-section">
     { listJobs}


 


<div className="process-comm">
<div className="spinner">
                    <div className="bounce1"></div>
                    <div className="bounce2"></div>
                    <div className="bounce3"></div>
</div>{/*end of spinner*/}
</div>{/*<!--process-comm end-->*/}
</div>{/*<!--jobs-section end-->*/}
    </div>
    <div className="col-4">
    <iframe
    allow="microphone;"
    width="350"
    height="430"
    src="https://rebaiahmed.github.io/mychatbot/">
</iframe>
    </div>
  </div>


  







      </div>
    );
    ;
  }
}

/*const mapStateToProps = ({ counter }) => ({
  count: counter.count,
  isIncrementing: counter.isIncrementing,
  isDecrementing: counter.isDecrementing
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      increment,
      incrementAsync,
      decrement,
      decrementAsync,
      changePage: () => push('/about-us')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)*/
