import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import{userService} from '../../services/user.service'; 
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { auth , database} from '../../firebase';
import _ from 'lodash';
import {User} from '../../components/UserComponent';


export default class Connections extends React.Component {


	constructor(props) {
		super(props);
		this.state = {
		  users:[],
			public_profile :{},
			currentUserID: "",

		  
		};

	   
	  }
		componentWillMount(){



	//______________GET THE CURRENTUSER INFORMATION____________________________//	
		auth.onAuthStateChanged(user =>{
				if(user){
						this.userRef = database.ref('users').child(auth.currentUser.uid);
						this.userRef.on("value", snap=>{
		
					 
					
								this.setState({username: snap.val().first + " " + snap.val().last});
								//_____GETE THE DATA FROM USER PROFILE_____________________//
								let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
							'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid }
							//____________SET STATE________________________________//
							this.setState({public_profile:userProfile})
						});
				}
		});
		
		
		
		//____reference to this____________//
		var that = this;
						//gets the user reference
		this.usersRef = database.ref().child('users').orderByChild("created_at");
		
		//___________________load the users__________________//
		this.usersRef.on("child_added", snap => {
			var user = snap.val();
			user.user_id = snap.ref.key;
			user.user_imgurl = "https://firebasestorage.googleapis.com/v0/b/testingproject-cd660.appspot.com/o/images%2Fdefault.jpg?alt=media&token=23d9c5ea-1380-4bd2-94bc-1166a83953b7";
		
		 //_____set the posts from database________________// 
			var updatedPusersArray = this.state.users;
			if(!user.email != this.state.public_profile.email)
			{
				updatedPusersArray.push(user);
			}
			
			this.setState({users :  updatedPusersArray});
		
			//____get the currentUser users!!________________//
		
			var userRef = database.ref('users/'+ user.user_id);
			userRef.once('value', snap=>{
		
					user.user_imgurl = snap.val().imageURL;
		
					var index = -1;
					for(var i = 0; i < this.state.users.length; i++){
							if(this.state.users[i].post_id == user.user_id ){
									index = i;
							}
					}
		
					var updatedPusersArray = this.state.users;
					updatedPusersArray.splice(index, 1, user);
					//____________________________________//
					updatedPusersArray  = _.reject(updatedPusersArray , (o) => {
						return o.email == this.state.public_profile.email;
					});
					//_________________________________________//
					this.setState({users: updatedPusersArray});
			});
		});


		}
	
		componentWillUnmount(){
			this.userRef.off();
	}

	
	  
		


	 //___________Render three users pages  in one Row_________________//
	 
	 renderusers() { 
		// two albums at a time - the current and previous item
		/*console.log('index is ' + index);
    let users = [this.state.users[index - 1], this.state.users[index]]; */
    let {users} =  this.state;
   /* return (
      <div className="columns" key={index}>
        {users.map( (user,index) => {
          return (
					 <user user={user} key={index} />
          );
        })}
      </div>
		);*/
		const rows =[];
		users.map((user,i)=>{
			
     rows.push(<User user={user} key={Math.random()} followUser={this.followUser} />)
		})

	
		
		//var rows = [<div className="col-md-4">content</div>, <div className="col-md-4">content</div>, <div className="col-md-4">content</div>]

  return _.chunk(rows, 3).map(function(group) { 


      return <div className="row">{group}</div>
		});
		
		
  }



	handleChange =(event) => {
    this.setState({value: event.target.value});
  }

  handleSubmit = (event) => {
    //alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
    
  
   let {users,value} = this.state ;
	 const results =[];
	 alert(value)
   //console.log('jobs are' + JSON.stringify(jobs));
   users.map((data,index)=>{
   
/*alert(data.first.toLowerCase().indexOf(value.toLowerCase() >=0))
alert( data.last.toLowerCase().indexOf(value.toLowerCase() >=0))
alert(data.headline.toLowerCase().indexOf(value.toLowerCase() >=0))*/
     if(data.first.toLowerCase().indexOf(value.toLowerCase()) >=0 || data.last.toLowerCase().indexOf(value.toLowerCase()) >=0
     || data.headline.toLowerCase().indexOf(value.toLowerCase()) >=0  )

     {
       
			 //add to result 
			 
			 results.push(data);
			 console.log('results' + data.first.toLowerCase().indexOf(value)
			 + '' +data.last.toLowerCase().indexOf(value) + ' ' + data.headline.toLowerCase().indexOf(value) );
     }
	 })//__end if map
	 
	 if(results.length>0)
	 {
		 alert("there are results" + JSON.stringify(results));
	 }else{

	 }
  this.setState({users:results});
   

  }


	followUser =(user)=>
	{
	;
		let {public_profile}=this.state ;
	
   var notificationData={
		 creation_date : new Date().getTime(),
		 content :public_profile.firstName + ' ' + public_profile.lastName + ' Just Followed you',
		 imageUrl : JSON.stringify(user.imageUrl),
		 forUser :JSON.stringify(user)
		}
		
		

		var newNotificationKey = database.ref().child('notifications').push().key;
		database.ref('/user-notifications/' + user.user_id + '/' + newNotificationKey).set(notificationData);

		toast.success("You Have succesfuly Followed this user !", {
			position: toast.POSITION.TOP_CENTER
		});
	}


	componentWillUnmount() {
    //this.socket.close()
    this.usersRef.off();
 }







  render() {

		let { users} = this.state ;
		let usersDisplay ;

		if(users.length >0)
		{
			/*{users.map((album, index) => {
				// use the modulus operator to determine even items return index % 2 ?
				this.renderAlbums(index) : '';
			})}*/
		}
    return (
      <section className="users-info">
			<div className="container">


    

			<div className="search-sec">
			<div className="container">
				<div className="search-box">
					<form onSubmit={this.handleSubmit}>
						<input value={this.state.value} onChange={this.handleChange}type="text" name="search" placeholder="Search users" />
						<button type="submit">Search</button>
					</form>
				</div>{/*<!--search-box end-->*/}
			</div>
		</div>{/*<!--search-sec end-->*/} 


				<div className="user-title">
					<h3>All users</h3>
				</div>{/*<!--user-title end-->*/}


				<div className="users-list">

				
					 {this.renderusers()}
				


				</div>{/*<!--users-list end-->*/}



				<div className="process-comm">
					<div className="spinner">
						<div className="bounce1"></div>
						<div className="bounce2"></div>
						<div className="bounce3"></div>
					</div>
        </div>{/*<!--process-comm end-->*/}
        

			</div>
		</section>
    );
    ;
  }
}

