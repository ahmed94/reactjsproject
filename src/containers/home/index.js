import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import io from "socket.io-client";
import { PublicProfile } from '../../components/PublicProfileComponent';
import  CreatePost  from '../../components/createPost';
import { RecommandedJobs } from '../../components/recommandedJobs';
import  Post  from '../../components/Post';
import { auth , database} from '../../firebase';
import{userService} from '../../services/user.service'; 


export default class Home extends React.Component {


  constructor(props) {
    super(props);
    
    this.state = {
      posts:[],
      recommanded_jobs:[],
      public_profile:{},
      username :''
    };

    //_____this.socket = io(URL_SOCKET);
    //this.socket = io('http://localhost:3000/');
   
  }


  componentWillMount() {

  




  //____GET THE currentUser username____________________//


auth.onAuthStateChanged(user =>{

 
    if(user){
        this.userRef = database.ref('users').child(auth.currentUser.uid);
        this.userRef.on("value", snap=>{

       
      
            this.setState({username: snap.val().first + " " + snap.val().last});
            //_____GETE THE DATA FROM USER PROFILE_____________________//
            let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
          'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid }
          //____________SET STATE________________________________//
          this.setState({public_profile:userProfile})
        });
    }
});



//____reference to this____________//
var that = this;
        //gets the post reference
this.postsRef = database.ref().child('posts').orderByChild("created_at");

//___________________load the posts__________________//
this.postsRef.on("child_added", snap => {
  var post = snap.val();
  post.post_id = snap.ref.key;
  post.user_imgurl = "https://firebasestorage.googleapis.com/v0/b/testingproject-cd660.appspot.com/o/images%2Fdefault.jpg?alt=media&token=23d9c5ea-1380-4bd2-94bc-1166a83953b7";

 //_____set the posts from database________________// 
  var updatedPostArray = this.state.posts;
  updatedPostArray.push(post);
  this.setState({posts : updatedPostArray});

  //____get the currentUser posts________________//

  var userRef = database.ref('users/'+ post.user_id);
  userRef.once('value', snap=>{

      post.user_imgurl = snap.val().imageURL;

      var index = -1;
      for(var i = 0; i < this.state.posts.length; i++){
          if(this.state.posts[i].post_id == post.post_id){
              index = i;
          }
      }

      var updatedPostArray = this.state.posts;
      updatedPostArray.splice(index, 1, post);
      this.setState({posts: updatedPostArray});
  });
});




 //__________for each child changed to post, replace that post with the post already in postArray
 this.postsRef.on("child_changed", snap => {
  var post = snap.val();
  post.post_id = snap.ref.key;

  var userRef = database.ref('users/'+ post.user_id);
  userRef.once('value', snap=>{

      post.user_imgurl = snap.val().imageURL;

      var index;
      for(var i = 0; i < this.state.posts.length; i++){
          if(this.state.posts[i].post_id == post.post_id){
              index = i;
          }
      }

      var updatedPostArray = this.state.posts;
      updatedPostArray.splice(index, 1, post);
      this.setState({posts: updatedPostArray});
  });
});



  }








  /*getUserInfo()
  {
   
    
    userService.getProfile(JSON.parse(localStorage.getItem('userId')))
    .then(res=>{
      console.log('res' + JSON.stringify(res));
      //____set the user ifnormation_____//
      this.setState({'public_profile':res});
    }).catch(err=>{
      console.log('err' + JSON.stringify(err));
    })

  }*/





  componentWillUnmount() {
   //this.socket.close()
   this.postsRef.off();
}



  likePost =(post) =>
  {
    
    //gets the ref of the user-likes to see if user has liked this post yet
    if(post.user_id != auth.currentUser.uid){
     
      var ref = database.ref('/user-likes/' + auth.currentUser.uid + '/' + post.post_id);
      ref.once('value', snap =>{

          //check if this data exists, and if it does, check if the user liked it
          if(snap.val() && snap.val().liked){

              //if user already liked this post, remove the user-likes reference
              var userLikesRef = database.ref('user-likes/' + auth.currentUser.uid + '/' + post.post_id);
              userLikesRef.remove();

              //decrementing likes of post
              post.likes-=1;
          }else{
              //if user hasn't yet liked this post, like it
              var likeUpdate = {};
              likeUpdate['/user-likes/' + auth.currentUser.uid + '/' + post.post_id] = {liked: true}
              database.ref().update(likeUpdate);

              //incrementing likes of post
              post.likes += 2;
          }

          var anotherPost = JSON.parse(JSON.stringify(post)); //copies contents of post into anotherPost
          delete anotherPost.post_id; //remove the post_id property in anotherPost -- we don't want to create an unnecessary post_id property in the post database

          //updates all the data in the posts ref and user-post ref
          var updates = {};
          updates['/posts/' + post.post_id] = anotherPost;
          updates['/user-posts/' + post.user_id + '/' + post.post_id] = anotherPost;
          database.ref().update(updates);
      });
}

  }

  CommentPost =(idPost,comment) =>
  {
    //alert('this post is commented!')
    let data ={'idPost':idPost,'idUser':0,'comment':comment}
    this.socket.emit('comment-post',{data})
   
  }

















  render() {
    let {public_profile ,posts ,username} = this.state ;
   let listPosts ;
    if(posts.length>0)
    {
     listPosts = posts.map((post) =>
    <Post post ={post} likePost={this.likePost} CommentPost={this.CommentPost} />
   
  );
    }else{
      listPosts = <h1>There is NO Posts to be displayed !</h1>
    }




    return (
      <div className="container">
     
  <div className="row">
    <div className="col-3">
      <PublicProfile userProfile={public_profile} />
     
    </div>
    <div className="col-6">
     <CreatePost  userProfile={public_profile} />

     	<div className="posts-section">

       
   
   	{ listPosts }


  <div className="process-comm">
		<div className="spinner">
												<div className="bounce1"></div>
												<div className="bounce2"></div>
												<div className="bounce3"></div>
	</div>{/*end of spinner*/}
		</div>{/*<!--process-comm end-->*/}
  </div>{/*<!--posts-section end-->*/}
  



    </div>{/*end of col lg6*/}
    <div className="col-3">
    <iframe
    allow="microphone;"
    width="350"
    height="430"
    src="https://rebaiahmed.github.io/mychatbot/">
</iframe>
    
   
    </div>
  </div>
</div>
    );
    ;
  }
}

/*const mapStateToProps = ({ counter }) => ({
  count: counter.count,
  isIncrementing: counter.isIncrementing,
  isDecrementing: counter.isDecrementing
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      increment,
      incrementAsync,
      decrement,
      decrementAsync,
      changePage: () => push('/about-us')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)*/
