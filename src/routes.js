//_________________//
import React  from 'react';


import App from './containers/app'
import Home from './containers/home/index'
import Companies from './containers/companies/companies';
import Connections from './containers/connections/connections';
import Jobs from './containers/jobs/jobs';
import Notifications from './containers/notifications/notifications';
import Profile from './containers/profile/profile';
import PublicCompanyPage from './containers/publiccompanypage/publiccompanypage';
import PublicProfilePage from './containers/publicprofile/publicprofile';
import SignInPage from './containers/signin/signin';
import SignUpPage from './containers/signup/signup';
import Messages from './containers/messages/messages';
import ModifyProfile  from './containers/modifyprofile/MyProfile';

import {isLoggedIn}  from './require_auth';
import history from './history'
import {  Router } from 'react-router';
import { Route, Switch ,Redirect} from 'react-router-dom'
import isAuthenticated from './firebase';


//_____________THE Not FoundRoute______________________//
const NotFound = ({ component: Component, ...rest }) => (
  
    <h1> 404 Not Found </h1>
  )


var routes = (
  <Router>
	  
   

      <Route exact path="/companies" component={Companies} />
      <Route  exact path="/connections" component={Connections} />
      <Route  exact path="/jobs" component={Jobs} />
      <Route  exact path="/notifications" component={Notifications} />
      <Route  exact path="/companyPage" component={PublicCompanyPage} />
      <Route  exact path="/profile" component={Profile} />
      <Route  exact path="/modifyprofile" component={ModifyProfile} />
      <Route  path="/publicprofile" component={PublicProfilePage} />
      <Route  exact path="/messages" component={Messages} />
      <Route exact path="/signin" component={SignInPage} />
      <Route exact path="/signup" component={SignUpPage} />
      {/*<Route component={NotFound} />*/}
    
    </Router>
);


const AuthorizedRoute = ({component: Component, ...rest}) => (
  <Route {...rest} render={renderProps => (
    isAuthenticated() ? (
          <Component {...renderProps} />
      ) : (
          <Redirect to='/signin' />
      )
  )}/>
);






const Routes = () => (
  <App>
      <Switch>
      <Route exact path="/" component={Home}  />
      <Route  exact path="/companies" component={Companies} />
      <Route  exact path="/connections" component={Connections} />
      <Route  exact path="/jobs" component={Jobs} />
      <Route  exact path="/notifications" component={Notifications} />
      <Route  exact path="/companyPage" component={PublicCompanyPage} />
      <Route  exact path="/profile" component={Profile} />
      <Route  exact path="/modifyprofile" component={ModifyProfile} />
      <Route  path="/publicprofile" component={PublicProfilePage} />
      <Route  exact path="/messages" component={Messages} />
      <Route exact path="/signin" component={SignInPage} />
      <Route exact path="/signup" component={SignUpPage} />
      </Switch>
  </App> )

export default  Routes;