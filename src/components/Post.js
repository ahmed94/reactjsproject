import React from 'react'
import Moment from 'react-moment';


export default class Post extends React.Component {


    constructor(props) {
      super(props);
      this.state = {
       newComment :'',
       comments:[],
       shouldHide:true,
       liked : false
      };
  
      //_____this.socket = io(URL_SOCKET);
      //this.socket = io('http://localhost:3000/');
     
    }

    /*componentWillUnmount() {
        this.socket.close()
    }*/


    handleChange =(event) => {
        this.setState({newComment: event.target.value});
      }


    //__________________like Post change Icon____//
    Like =()=>{
        
    }


    //___________________________//
    addComment (){
        
        //this.setState({'shouldHide':'shouldHide' =!'shouldHide'});
        this.setState({shouldHide: !this.state.shouldHide})
    } 


    Comment =(e)=>{

        e.preventDefault()
        //alert("clicked")
    if(this.state.newComment)
    {
        let comment =
        {'content':this.state.newComment,
        'author':'currentUser','time':new Date()}
        /*this.state.comments.push({'content':this.state.newComment,
        'author':'currentUser','time':new Date()})*/
       /*this.setState(
        prevState => ({
          comments: [...prevState.names, this.state.newComment]
        }))*/

        this.setState({
            comments: [
                ...this.state.comments,
                comment
            ]
        })

        //____se the newComment ot empty'
        this.setState({'newComment':''});

        console.log('coment swill be' + this.state.comments);
      //send the new Comment to the back-end_____________//
      this.props.CommentPost(1,comment)  
    }
     
    
    }



    render() {

        const  shouldHide = this.state.shouldHide;
        let add;
        let Com ;
        let {post} = this.props ;
    



        //____comments_________________//

        const listCommnets = this.state.comments.map((comment) =>
        <div className="comment-list">
        <div className="bg-img">
            <img src="images/resources/bg-img3.png" alt="" />
        </div>
        <div className="comment">
           <h3>John Doe</h3>
            <span><img src="images/clock.png" alt="" /> 3 min ago</span>
            <p>endrerit metus, ut ullamcorper quam finibus at.</p>
                                                                
            </div>
    </div>
);

    
        if (!shouldHide) {
          add =  <div className="comment_box">
          <form  onSubmit={this.Comment}>
              <input onChange={this.handleChange} value={this.state.newComment} type="text" placeholder="Post a comment" />
              <button  type="submit">Send</button>
              </form>
          </div>;
        } else {
          add = null;
        }

       






        return (
            <div className="post-bar">
    <div className="post_topbar">
        <div className="usy-dt">
            <img src={post.user_imgurl} alt="" />
            <div className="usy-name">
                <h3>{post.user_name }</h3>
                <span><img src="images/clock.png" alt="" /><Moment unix>{post.created_at } </Moment></span>
            </div>
        </div>
        <div className="ed-opts">
            <a href="#" title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></a>
        
        </div>
    </div>
    <div className="epi-sec">
        <ul className="descp">
            <li><img src="images/icon8.png" alt="" /><span>Location</span></li>
          
        </ul>
        <ul className="bk-links">
            <li><a href="#" title=""><i className="la la-bookmark"></i></a></li>
            <li><a href="#" title=""><i className="la la-envelope"></i></a></li>
        </ul>
    </div>
    <div className="job_descp">
       
        
        <p>{post.body}</p>
       
    </div>
    <div className="job-status-bar">
        <ul className="like-com">
            <li>
            <a onClick={()=>this.props.likePost(post)}> <i className="la la-heart"></i> Like</a>
           
           { /* <a onClick={()=>this.props.likePost(1)}><i className="la la-heart"></i> Liked</a>
                <img src="images/liked-img.png" alt="" />
        <span>{post.likes}</span>*/}
        </li> 
        <span>{post.likes}</span>
           {/* <li><a onClick={()=>this.addComment()} title="" className="com"><img src="images/com.png" alt="" /> Comment 15</a></li>*/}
        </ul>
        <a><i className="la la-eye"></i>Views 50</a>
        { add }
      
        </div>

        <div className="comment-section">
         {listCommnets}
        </div>
    </div>
    

        )

    }

}    



