import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import Dropzone from 'react-dropzone'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import{userService} from '../services/user.service'; 

import { auth , database, time} from '../firebase';
export default class CreateCompanyPage extends React.Component {
    


    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          modal2:false,
          tags: [],
          files :[],
          username:'',
          description:'',
            companyName:'',
            startDate:'',
            website:''

          
        };
    
        this.toggle = this.toggle.bind(this);
        
        this.handleChange =this.handleChange.bind(this);
      }




    componentWillMount(){
      auth.onAuthStateChanged(user =>{
        if(user){
            this.userRef = database.ref('users').child(auth.currentUser.uid);
            this.userRef.on("value", snap=>{
                this.setState({username: snap.val().first + " " + snap.val().last});
                this.setState({imageUrl :  snap.val().imageUrl});
            });
        }
});


      }


  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

//______________________________________//
validateField(fieldName, value) {
  let fieldValidationErrors = this.state.formErrors;
  let emailValid = this.state.emailValid;
  let passwordValid = this.state.passwordValid;
  
  switch(fieldName) {
    case 'email':
    emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    fieldValidationErrors.email = emailValid ? '' : ' is invalid';
    break;
    case 'password':
    passwordValid = value.length >= 6;
    fieldValidationErrors.password = passwordValid ? '': ' is too short';
    break;
    default:
    break;
  }
  this.setState({formErrors: fieldValidationErrors,
          emailValid: emailValid,
          passwordValid: passwordValid
          }, this.validateForm);
  }


  //_________________________________//
  errorClass(error) {
		return(error.length === 0 ? '' : 'has-error');
  }
  
  //__________________________________________________//
  handleUserInput (e) {
		const name = e.target.name;
    const value = e.target.value;
		this.setState({[name]: value});
	  }	


//___________________//
handleChange(tags) {
    this.setState({tags})
  }  

//___________________//
  onDrop(files) {
    this.setState({files});
    console.log('files' + JSON.stringify(files))
  }
//___________________//
  onCancel() {
    this.setState({
      files: []
    });
  }



  //________CreateCompanyPage functions*****//

  createCompanyPage =(e)=>
  {
    e.preventDefault();

   
  /*const data = new FormData();
  //__append all files
  if(this.state.files)
  {
    this.state.files.forEach(file => {
      //console.log('filename without space' + this.hasWhiteSpace(file.name))
     
     data.append('file', file);
     data.append('filename', this.hasWhiteSpace(file.name));
     data.append('date' ,new Date())
    });
  }

  //____GET THE DATA_______________//
  data.append('companyName',this.state.companyName);
  data.append('description',this.state.description);
  data.append('website',this.state.description);
  data.append('startDate',this.state.startDate);*/

  let { username ,companyName ,description,website,startDate} =this.state ;
 

  var companyData = {
    user_id: auth.currentUser.uid,
    user_name: username,
    companyName :companyName,
    description :description,
    website:website,
    startDate : startDate,
    created_at: new Date().getTime(),
    imageUrl : "https://ui-avatars.com/api/?name="+companyName,
    followers :[]
};

//generate new post reference key
var companyRefKey = database.ref().child('companies').push().key;
//sets the postData to the post child with the postRefKey
database.ref('companies/' + companyRefKey).set(companyData);
//sets the postData to the user-posts child with the currentUserId & the postRefKey
database.ref('/user-companies/' + auth.currentUser.uid + '/' + companyRefKey).set(companyData);

this.toggle();

  }



 
    render() {
      let { imageUrl} = this.state ;
      

      const files = this.state.files.map(file => (
        <li key={file.name}>
          {file.name}
        </li>
      ))


      return(
          
      <div className="post-topbar">
  <div className="user-picy">
      <img src={imageUrl} alt="" />
  </div>
  <div className="post-st">
      <ul>
          <li><a onClick={this.toggle} className="post_project"  title="">Create a Company Page</a></li>
      </ul>
  </div>{/*<!--post-st end-->*/}

  <ToastContainer /> 

  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Create a CompanyPage </ModalHeader>
          <ModalBody>
          
          <div class="post-project-fields">
              <form>
              <div class="form-group">
                                  <label for="exampleInputEmail1">Company Name</label>
                                  <input onChange={(event) => this.handleUserInput(event)}  name="companyName"  type="text" class="form-control" 
                                 
                                   placeholder="Enter Company Name"
                                   value={this.state.companyName} />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Start Date</label>
                                    <input  value={this.state.startDate}
                                     name="startdate"   type="date" class="form-control" id="date" 
                                     onChange={event => this.setState({startDate: event.target.value})}  placeholder="Enter start Date" />
                                  </div>


                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Company Website</label>
                                      <input  value={this.state.website}
                                       name="website" onChange={(event) => this.handleUserInput(event)}   type="text" class="form-control" id="website" aria-describedby="emailHelp" placeholder="Enter Company Website" />
                                    </div>



                                <div class="form-group">
                                    <label for="exampleInputEmail1">About the Company </label>
                                    <textarea class="form-control" 
                                     value={this.state.description} onChange={(event) => this.handleUserInput(event)}  name="description" id="about" rows="3"></textarea>
                                  </div>

                                  <div className="form-group">
            

            
            

            <Dropzone
         onDrop={this.onDrop.bind(this)}
         onFileDialogCancel={this.onCancel.bind(this)}
         multiple={false}
         accept="image/*"
       >
         {({getRootProps, getInputProps}) => (
           <div {...getRootProps()}>
             <input {...getInputProps()} />
             <label className="btn btn-default btn-file">
              Image <i className="fa fa-camera"></i> 
             
             
            </label>
            <ul>{files}</ul>
              
           </div>
         )}
       </Dropzone>

            </div>
              </form>
            </div>{/*<!--post-project-fields end-->*/}

             
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.createCompanyPage}>Create</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>




</div>/*<!--post-topbar end-->*/ );
    }
  }