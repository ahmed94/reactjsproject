import React from 'react'
 

export const PublicProfile = (props) => 

   
    (
    
    <div className="user-data full-width">
 <div className="user-profile">
     <div className="username-dt">
         <div className="usr-pic">
             <img src={props.userProfile.imageUrl} alt="" />
         </div>
     </div>{/*<!--username-dt end-->*/}
     <div className="user-specs">
         <h3> { props.userProfile.firstName + ' ' +  props.userProfile.lastName }</h3>
         <span>{ props.userProfile.email } </span>
     </div>
 </div>{/*<!--user-profile end-->*/}
 <ul className="user-fw-status">
     <li>
         <h4>Following</h4>
         <span>34</span>
     </li>
     <li>
         <h4>Followers</h4>
         <span>155</span>
     </li>
     <li>
    
     </li>
 </ul>
</div>
    );
  