

import React from 'react'
 

export const User = (props) => 

   
    (
    
        <div className="col-lg-3 col-md-4 col-sm-6">
        <div className="company_profile_info">
            <div className="company-up-info">
                <img src={props.user.imageUrl} alt="" />
                <h3>{ props.user.first + ' ' + props.user.first  }</h3>
                <h4> {props.user.headline}</h4>
                <ul>
                    <li><a onClick={ ()=>props.followUser(props.user)} title="" className="follow">Follow</a></li>
                    <li><a href="#" title="" className="message-us"><i className="fa fa-envelope"></i></a></li>
                </ul>
            </div>
           
        </div>{/*<!--company_profile_info end-->*/}
    </div>
    );
  