
import React from 'react'
import Moment from 'react-moment';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Card,CardBody,CardText,CardSubtitle,CardTitle } from 'reactstrap';
export const Experience = (props) => 

   
    (
    
        <Card body className="text-center">
<CardTitle>{props.experience.newExperienceTitle} </CardTitle>
      <CardSubtitle>{props.experience.newExperienceCompany}</CardSubtitle>
<CardText>{props.experience.newExperienceDescription} </CardText>
<h5>  {props.experience.newExperienceStartDate}  </h5>
<h5> {props.experience.newExperienceEndDate}  </h5> 
</Card>
    );