import React from 'react'
import { Button} from 'reactstrap';

export const JobOffer = (props) => (
   
	<div className="post-bar">
    <div class="post_topbar">
        <div class="usy-dt">
            <img src={props.job.user_imgurl} alt="" />
            <div className="usy-name">
                <h3>{props.job.companyPage }</h3>
                <span><img src="images/clock.png" alt="" />3 min ago</span>
            </div>
        </div>
        <div className="ed-opts">
            <a href="#" title="" className="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
        
        </div>
    </div>
    <div className="epi-sec">
        <ul className="descp">
            <li><img src="images/icon8.png" alt="" /><span>Location</span></li>
          
        </ul>
        <ul className="bk-links">
            <li><a href="#" title=""><i className="la la-bookmark"></i></a></li>
            <li><a href="#" title=""><i className="la la-envelope"></i></a></li>
        </ul>
    </div>
    <div className="job_descp">
	<h3>{props.job.title}</h3>
		<ul className="job-dt">
		<li><a href="#" title="">{props.job.jobType}</a></li>
		<li><span>$ {props.job.price} / hr</span></li>
		</ul>
		<p> {props.job.description}</p>
		<ul className="skill-tags">
		
		{props.job.tags.map((tag, i) => {     
                           
           // Return the element. Also pass key     
           return (<li><a href="#" title="">{tag}</a></li>) 
        })}

		</ul>
	</div>{/*end of job dezscription*/}
    <div className="job-status-bar">
        <ul className="like-com">
            <li>
            <Button onClick={ ()=>props.ApplyJob(1)} color="primary">Apply <i className="la la-bookmark"></i></Button>{' '}
               
            </li> 
            
        </ul>
      
    </div>
</div>/*<!--post-bar end-->*/
);