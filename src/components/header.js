import React from 'react'
import { Link } from 'react-router-dom';
import {Redirect} from 'react-router';
import createHistory from 'history/createBrowserHistory';

import { Nav, NavItem, NavLink,Dropdown,DropdownItem,DropdownMenu,DropdownToggle } from 'reactstrap';
import{userService} from '../services/user.service'; 
import { auth } from '../firebase';



export default class Header extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            dropdownOpen: false,
            navigate: false
        };
    
        //this.socket = io(URL_SOCKET);
        this.toggle = this.toggle.bind(this);
        this.logout =this.logout.bind(this);
       
      }

      toggle() {
        console.log('changed')
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }


    //___________LOGOUT function__________//
    logout= (e)=>{
        //e.preventDefault();
        console.log('logout man');
        userService.logout();
        //alert("hheheh")
        this.setState({ navigate: true })
    
    }

    

      render() {

        const { navigate } = this.state
        let { username} =this.props;

        
        

        // here is the important part
        if (navigate) {
          return <Redirect to="/" push={true} />
        }
        return (
            <div>
            <header>
            <div className="container">
                <div className="header-data">
                    <div className="logo">
                        <a href="/" title=""><img src="images/logo.png" alt="" /></a>
                    </div>
                  
                    <nav>
                        <ul>
                            <li>
                            <Link to="/" style={{'textDecoration': 'none'}}>
                                    <span><img src="images/icon1.png" alt="" /></span>
                                    Home 
                                    </Link> 
                            </li>
                            <li>
                            <Link to="/companies" style={{'textDecoration': 'none'}}>
                                    <span><img src="images/icon2.png" alt="" /></span>
                                    Companies
                              </Link> 
                               
                            </li>


                            <li>
                            <Link to="/connections" style={{'textDecoration': 'none'}}>
                                    <span><img src="images/icon2.png" alt="" /></span>
                                    Connections
                              </Link> 
                               
                            </li>
                           
                          
                            <li>
                            <Link to="/jobs" style={{'textDecoration': 'none'}}>
                                    <span><img src="images/icon5.png" alt="" /></span>
                                    Jobs 
                               </Link>
                            </li>
                        
                            <li>
                            <Link to="/notifications" style={{'textDecoration': 'none'}}>
                                    <span><img src="images/icon7.png" alt="" /></span>
                                    
                                    Notifications
                            </Link> 
                             
                            </li>

                           
        
                            <li>
                            <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                    <DropdownToggle nav caret>
                     { username }
                    </DropdownToggle>
                    <DropdownMenu>
                    <Link style={{'textDecoration': 'none'}} to="/modifyprofile"><DropdownItem>Profile</DropdownItem></Link>
                      <DropdownItem divider />
                      <DropdownItem onClick={this.props.logout}>logout</DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                            </li>
        
               
                        </ul>
                    </nav>
                    
                 
                </div>
            </div>
        </header>
        </div>
        )
    }

}



