import React from 'react'
import Moment from 'react-moment';
export const NotificationComponent = (props) => (
    <div className="notfication-details">
    <div className="noty-user-img">
      <img src={props.notification.forUser.imageUrl} alt="" />
    </div>
    <div className="notification-info">
      <h3><a href="#" title="">{props.notification.content} </a></h3>
      <span><Moment unix>{props.notification.creation_date}</Moment></span>
    </div>{/*<!--notification-info -->*/}
  </div>

);