/* jshint expr: true */
import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'

import { auth , database, time} from '../firebase';
import lunr from 'lunr';

export default class CreateOffer extends React.Component {
    


    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          modal2:false,
          tags: []
        };
    
        this.toggle = this.toggle.bind(this);
        this.toggle2=this.toggle2.bind(this);
        this.handleChange =this.handleChange.bind(this);

        this.idx = lunr(function () {
          this.ref('job_id')
          this.field('description')
          this.field('tags')
          this.field('title')
        
          /*documents.forEach(function (doc) {
            this.add(doc)
          }, this)*/
        })
      }


  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

//___open the second modal*/
toggle2() {
    this.setState({
      modal2: !this.state.modal2
    });
  }

//___________________//
handleChange(tags) {
    this.setState({tags})
  }  




  //_____________________________________//
  handleUserInput (e) {
		const name = e.target.name;
    const value = e.target.value;
		this.setState({[name]: value});
	  }	  


  //________________________________//
  
	
		
	  


   //________CreateCompanyPage functions*****//

   createOffer =(e) =>
   {
     //e.preventDefault();
     let{ title ,category,tags,jobType,description,price} = this.state ;
 console.log('category is' + category);

     var offerData = {
      user_id: auth.currentUser.uid,
      user_name: auth.currentUser.first + ' ' +  auth.currentUser.last ,
      companyPage : 'Jobi.tn ',
      title :title,
      category :category,
      tags : tags,
      price :price,
      jobType : jobType,
      description:description,
      created_at: new Date().getTime(),
      applies :[],
  };

  console.log('the user offer ' + JSON.stringify(offerData));

  //generate new post reference key
  var offerRefKey = database.ref().child('jobs').push().key;
  //sets the postData to the post child with the postRefKey
  database.ref('jobs/' + offerRefKey).set(offerData);
  //sets the postData to the user-posts child with the currentUserId & the postRefKey
  database.ref('/user-offers/' + auth.currentUser.uid + '/' + offerRefKey).set( offerData);

  //____add the job offer ____//

  //emptys the post text field
  this.toggle();
 

   }

 
    render() {
      let { imageUrl}=this.props ;
      return(
          
      <div className="post-topbar">
  <div className="user-picy">
      <img src={imageUrl} alt="" />
  </div>
  <div className="post-st">
      <ul>
          <li><a onClick={this.toggle} className="post_project"  title="">Post an Offer</a></li>
      </ul>
  </div>{/*<!--post-st end-->*/}

  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Create an Offer </ModalHeader>
          <ModalBody>
          
          <div class="post-project-fields">
              <form>
                <div class="row">
                  <div class="col-lg-12">
                    <input type="text" onChange={(event) => this.handleUserInput(event)} value={this.state.title} name="title" placeholder="Title" />
                  </div>
                  <div class="col-lg-12">
                    <div class="inp-field">
                      <select onChange={(event) => this.handleUserInput(event)} value={this.state.category}
                      name="category">
                        <option value="Web Developement">Web Developement </option>
                        <option value="Mobile Developement">Mobile Developement</option>
                        <option value="Web & Mobile Design">Web & Mobile Design</option>
                        <option value="Data Science & Analytics">Data Science & Analytics</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    
                    <TagsInput value={this.state.tags} onChange={this.handleChange} />
                  </div>
                  <div class="col-lg-6">
                    <div class="price-br">
                      <input onChange={(event) => this.handleUserInput(event)} value={this.state.price} type="text" name="price" placeholder="Price" />
                      <i class="la la-dollar"></i>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="inp-field">
                      <select name="jobType" onChange={(event) => this.handleUserInput(event)} value={this.state.jobType}>
                        <option value="Full Time">Full Time</option>
                        <option value="Half time">Half time</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <textarea onChange={(event) => this.handleUserInput(event)} value={this.state.description} name="description" placeholder="Description"></textarea>
                  </div>
                
                </div>
              </form>
            </div>{/*<!--post-project-fields end-->*/}

             
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.createOffer}>Post</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>




</div>/*<!--post-topbar end-->*/ );
    }
  }