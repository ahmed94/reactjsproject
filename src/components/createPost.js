import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Dropzone from 'react-dropzone'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import{userService} from '../services/user.service'; 

import { auth , database, time , storage} from '../firebase';


export default class CreatePost extends React.Component {
    


    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          modal2:false,
          postContent :'',
          files:[],
          imageURL: '',
          postLink :'',
          public_profile:{}
        };
    
        this.toggle = this.toggle.bind(this);
        this.toggle2=this.toggle2.bind(this);
        this.handleChange = this.handleChange.bind(this);
      }


  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }


  componentWillMount() {

  




    //____GET THE currentUser username____________________//
  
  
  auth.onAuthStateChanged(user =>{
      if(user){
          this.userRef = database.ref('users').child(auth.currentUser.uid);
          this.userRef.on("value", snap=>{
  
         
        
              this.setState({username: snap.val().first + " " + snap.val().last});
              //_____GETE THE DATA FROM USER PROFILE_____________________//
              let userProfile ={ 'firstName' : snap.val().first ,'lastName' :snap.val().last ,
            'imageUrl' :snap.val().imageUrl ,'email': snap.val().email ,'userId' : auth.currentUser.uid }
            //____________SET STATE________________________________//
            this.setState({public_profile:userProfile})
          });
      }
  });


}


  /*getUserInfo()
  {
   
    
    userService.getProfile(JSON.parse(localStorage.getItem('userId')))
    .then(res=>{
      console.log('res' + JSON.stringify(res));
      //____set the user ifnormation_____//
      this.setState({'public_profile':res});
    }).catch(err=>{
      console.log('err' + JSON.stringify(err));
    })

  }*/

//___open the second modal*/
toggle2() {
    this.setState({
      modal2: !this.state.modal2
    });
  }


  //_____________________________//

  handleChange(event) {
    this.setState({postContent: event.target.value});
  }




  //_____________________________//

  handleChange2 =(event) => {
    this.setState({postLink: event.target.value});
  }

  //_____________________________//
  
  publishPoste =(e)=>
  {
    e.preventDefault();

   
  const data = new FormData();
  //__append all files
/*  if(this.state.files)
  {
    alert("there is files" +JSON.stringify(this.state.files));
    const imageUpload = storage.ref(`images/${this.state.files.name}`).put(this.state.files);
    imageUpload.on('state_changed', (snapshot) => {
        //process fun here  
          console.log(snapshot);
        }
      ,
      (error) => {
         //error fun here 
          console.log(error);       
        }
      ,
      () => {
        storage
          .ref('images')
          .child(this.state.files.name)
          .getDownloadURL()
          .then((url) => {
            console.log(url);
            alert("uplaoded")            
          }).catch(err=>{
            alert("err")
          })
        }
       )

    console.log('we have file' + JSON.stringify(this.state.files))
    this.state.files.forEach(file => {
    console.log('filename without space' + file.name)
     
    
 
    });
  }*/

  if(this.state.postContent)
  {
    console.log('postconet' + this.state.postContent)
    //data.append('text',this.state.postContent);

    //___get the data frm props____//
    let { public_profile } = this.state ;


  

    //gathers the data from the post submission
    var postData = {
      user_id: public_profile.userId,
      user_name: public_profile.firstName + ' ' +  public_profile.lastName,
      body: this.state.postContent,
      created_at: time,
      replies: [],
      likes: 0
  };
   //generate new post reference key
   var postRefKey = database.ref().child('posts').push().key;
   //sets the postData to the post child with the postRefKey
   database.ref('posts/' + postRefKey).set(postData);
   //sets the postData to the user-posts child with the currentUserId & the postRefKey
   database.ref('/user-posts/' + auth.currentUser.uid + '/' + postRefKey).set(postData);
   this.setState({'postContent':""});
  }


  

  /*userService.createPost(data)
  .then(res=>{
    console.log('post create duscesfuly' + JSON.stringify(res));
    this.toggle();
    toast.success("Your Post published Succesfuly", {
      position: toast.POSITION.TOP_CENTER
    });

  }).catch(err=>{
    console.log('err creating post' + err)
    toast.error("Error creating POST !", {
      position: toast.POSITION.TOP_CENTER
    });
  })*/

  
  

  


 
  }


//_______________________________________________//

PublishPostLink =(e)=>
{
  e.preventDefault();



  //only saves data if the post field isn't empty
  if(this.refs.body.value){
    //gathers the data from the post submission
    var postData = {
        user_id: auth.currentUser.uid,
        user_name: this.state.username,
        body: this.refs.body.value,
        created_at: time,
        replies: [],
        likes: 0
    };

    //generate new post reference key
    var postRefKey = database.ref().child('posts').push().key;
    //sets the postData to the post child with the postRefKey
    database.ref('posts/' + postRefKey).set(postData);
    //sets the postData to the user-posts child with the currentUserId & the postRefKey
    database.ref('/user-posts/' + auth.currentUser.uid + '/' + postRefKey).set(postData);

    //emptys the post text field
    this.refs.body.value = "";
   //_____close the modal______________//
    this.toggle()
    toast.success("Your Post published with success !", {
      position: toast.POSITION.TOP_CENTER
    });
}



}




  onDrop(files) {
    this.setState({files});
    console.log('files' + JSON.stringify(files))
  }

  onCancel() {
    this.setState({
      files: []
    });
  }
 
    render() {

      let { public_profile} = this.state ;

      const files = this.state.files.map(file => (
        <li key={file.name}>
          {file.name}
        </li>
      ))

      return(
          
      <div className="post-topbar">
  <div className="user-picy">
      <img src={public_profile.imageURL} alt="" />
  </div>
  <div className="post-st">
      <ul>
          <li><a onClick={this.toggle} className="post_project"  title="">Post a Publication</a></li>
          <li><a onClick={this.toggle2} className="post-jb active"  title="">Post a Link</a></li>
      </ul>
  </div>{/*<!--post-st end-->*/}

  <ToastContainer /> 

  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Post a Publication </ModalHeader>
          <ModalBody>
          <div className="form-group">
           <label htmlFor="comment">Publish a Content:</label>
            <textarea onChange={this.handleChange}
             value={this.state.postContent} className="form-control" rows="5" id="comment"></textarea>
            </div> 

             <div className="form-group">
            

            
            

             <Dropzone
          onDrop={this.onDrop.bind(this)}
          onFileDialogCancel={this.onCancel.bind(this)}
          multiple={false}
          accept="image/*"
        >
          {({getRootProps, getInputProps}) => (
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              <label className="btn btn-default btn-file">
               Image <i className="fa fa-camera"></i> 
              
              
             </label>
             <ul>{files}</ul>
               
            </div>
          )}
        </Dropzone>

             <div>
          
                </div>
            
              
      
            </div> 
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.publishPoste}>Publish</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>

{/*Modal to adda Link*/}
 <Modal isOpen={this.state.modal2} toggle={this.toggle2} className={this.props.className}>
          <ModalHeader toggle={this.toggle2}>Post a Link </ModalHeader>
          <ModalBody>
          <div className="form-group">
          <label >Link </label>
          <input onChange={this.handleChange2}
             value={this.state.postLink} type="text" placeholder="Link URL" />
          </div>
         
             
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.PublishPostLink}>Publish</Button>{' '}
            <Button color="secondary" onClick={this.toggle2}>Cancel</Button>
          </ModalFooter>
        </Modal>



</div>/*<!--post-topbar end-->*/ );
    }
  }