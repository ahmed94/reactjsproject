

import React from 'react'
 

export const Company = (props) => 

   
    (
    
        <div className="col-lg-3 col-md-4 col-sm-6">
        <div className="company_profile_info">
            <div className="company-up-info">
                <img src={props.company.imageUrl} alt="" />
                <h3>{ props.company.companyName }.</h3>
                <h4>Establish {props.company.startDate}</h4>
                <ul>
                    <li><a onClick={ ()=>props.followCompany(0)} title="" className="follow">Follow</a></li>
                    <li><a href="#" title="" className="message-us"><i className="fa fa-envelope"></i></a></li>
                </ul>
            </div>
            <p className="view-more-pro">
            { props.company.description}
            </p>
        </div>{/*<!--company_profile_info end-->*/}
    </div>
    );
  