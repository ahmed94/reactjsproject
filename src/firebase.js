import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCtbEbw_tJ-61hXSKO_GSYExyHMXkaKKkw",
    authDomain: "isammwebcamp.firebaseapp.com",
    databaseURL: "https://isammwebcamp.firebaseio.com",
    projectId: "isammwebcamp",
    storageBucket: "isammwebcamp.appspot.com",
    messagingSenderId: "430129076037"



};

firebase.initializeApp(config);

export default firebase;

export const database = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const messaging = firebase.messaging();
export const dbstore = firebase.firestore();
export const time =firebase.database.ServerValue.TIMESTAMP;

export const isAuthenticated = () => {
    return !!auth.currentUser 
  };
