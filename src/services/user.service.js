
import { authHeader } from './helpers';

export const userService = {
    login,
    logout,
    register,
    getMyNotifications,
    getPosts,
    getProfile,
    createPost,
    createCompanyPage,
    getCompaniesPages,
    getOffers,
    getById,
    SearchOffers,
    update,
    delete: _delete
};


const API_URL ='http://192.168.1.28:8000'
//__________________________________________//

function login(email, password) {
    console.log('ema' + email + 'pas' + password)
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 'email':email, 'password':password })
    };
    

    return fetch(`${API_URL}/user/login`, requestOptions)
        .then(user => user.json())
        //.then(handleResponse)
        
}


//______________________________________________________________//


function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${API_URL}/user/register`, requestOptions);
}


//______________________________________________________________//


function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

//______________________________________________________________//



function getMyNotifications() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_URL }/mynotificatio,ns`, requestOptions).then(handleResponse);
}


//______________________________________________________________//

function getPosts() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_URL }/posts/getAll`, requestOptions).then(res=>res.json());
}


//______________________________________________________________//




//______________________________________________________________//


function createPost(post) {
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(post)
    };

    return fetch(`${API_URL }/posts/create`, requestOptions).then(response => {
        response.json();
        
});
}


//_________________________________________________//


function createCompanyPage(company) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(company)
    };

    return fetch(`${API_URL}/company`, requestOptions).then(handleResponse);
}





//__________________________________________________________________//

function getOffers() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_URL }/offers`, requestOptions).then(handleResponse);
}



//______________________________________________________________//


function SearchOffers(keywords) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(keywords)
    };

    return fetch(`${API_URL}/offers/search`, requestOptions).then(handleResponse);
}


//______________________________________________________________//

function getRecommandedOffers() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_URL }/posts`, requestOptions).then(handleResponse);
}

//______________________________________________________________//

function getCompaniesPages() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_URL }/posts`, requestOptions).then(handleResponse);
}


//______________________________________________________________//

function getProfile(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_URL}/user/get/${id}`, requestOptions).then(res=>res.json());
}



function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_URL .apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}




function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${API_URL .apiUrl}/users/${user.id}`, requestOptions).then(handleResponse);;
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${API_URL}/user/update/`, requestOptions).then(handleResponse);
}

function handleResponse(response) {

    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}