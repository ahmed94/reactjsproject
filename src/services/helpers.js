export function authHeader() {
    // return authorization header with jwt token
    let token = JSON.parse(localStorage.getItem('userToken'));
    console.log('user geted token ' + JSON.stringify(token));

    if (token) {
        return { 'Authorization':token};
    } else {
        return {};
    }
}


